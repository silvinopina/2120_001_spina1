import java.io.IOException;
import java.lang.IllegalStateException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
/**
* A simple program that takes in a
* Serialized file and writes in
* a student database
*
* @author Silvino Pina
*
*/
public class StudentDatabaseSerializableFileWriter {
  //Creats an ObjectOutputStream
  static ObjectOutputStream output = null;

  //Takes in a file and trys to open it
  public static void openFile(String filename) {
		try{
			output =  new ObjectOutputStream(new FileOutputStream(filename));
		}
    //Thrown when no contents are found in the file
    catch (FileNotFoundException e){
			System.err.println("Error opening file for writing.");
		}
    //Thrown when opening is interrupted
		catch (IOException e) {
			System.err.println("Error opening header for writing.");
		}
	}//end openFile method

  //Attempts to write objects into Serialized file
	public static void writeData(StudentDatabase db) {
		try {
      for (int i=0; db.getNumStudents() > i; ++i){
        output.writeObject(db.getStudent(i));
      }


		}
		catch (NullPointerException e){
			System.err.println("File for saving not properly initialized.");

		}
    //Thrown when writing is interrupted
    catch (IOException e){
    			System.err.println("Error writing objects to save file.");
    			e.printStackTrace();

    }
	}//End writeData method

  //Attemps to close the file
	public static void closeFile() {
    try {
      output.close();
    }
    //Thrown when closing is interrupted
    catch (IOException e) {
      System.err.println("Error closing save file after write.");
    }

	}//End closeFile Method
}//end StudentDatabaseSerializableFileWriter
