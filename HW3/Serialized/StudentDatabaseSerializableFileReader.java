import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.io.EOFException;
import java.util.Scanner;
/**
* A simple program that takes in a
* Serialized file that contains a student database
* and reads its data
*
* @author Silvino Pina
*
*/
public class StudentDatabaseSerializableFileReader{
  //Creats an ObjectInputStream
   static ObjectInputStream deserializationInput = null;

   //Takes in a file and trys to open it
    public static void openFile(String filename) {
  		try{
  		    deserializationInput =  new ObjectInputStream(new FileInputStream(filename));
  		}
      //Thrown when no contents are found in the file
  		catch (FileNotFoundException e){
  			System.err.println("Error opening save file for reading.");
  		}
      //Thrown when opening is interrupted
  		catch (IOException e) {
  			System.err.println("Error opening header for reading objects.");
  		}
  	} // end method openFile


  	//Attempts to read objects in the Serialized file
  	public static StudentDatabase readObjects() {
      //New StudentDatabase that will be created with data from file
      StudentDatabase db = new StudentDatabase();
      try{
        while(true){
          db.add((Student)deserializationInput.readObject());
        }
      }
      //Thrown when end of file is reached
  		catch (EOFException e){
  			System.out.println("Completed reading save file.");
  		}
      //Thrown when no contents are found in the file
  		catch(ClassNotFoundException e){
  			System.err.println("Contents of save file corrupted.");
  		}
      //Thrown when reading is interrupted
  		catch (IOException e){
  			System.err.println("I/O Exceptipon during reading of file.");
  			e.printStackTrace();
  		}

      //returns the student database
      return db;

  	} // end method readObjects


  	//Attemps to close the file
  	public static void closeFile() {
  		try {
  			deserializationInput.close();
  		}
      //Thrown when closing is interrupted
  		catch (IOException e) {
  			System.err.println("Error closing save file after read.");
  		}
  	} // end method closeFile
  } // end class StudentDatabaseSerializableFileReader
