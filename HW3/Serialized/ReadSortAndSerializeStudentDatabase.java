import java.util.ArrayList;
/**
* A simple program that takes in a
* Serialized file that contains a student database
* and converts it to a txt file
*
* @author Silvino Pina
*
*/
public class ReadSortAndSerializeStudentDatabase {

  //Main method that converts a Serialized file into a CSV file
    public static void main(String[] args) {
      //Checks if correct amount of arguments are passed in
        if (args.length != 2) {
            System.err.println("Usage: BuildAndSortStudentDataFromSerializedFile inputfilename outputfilename");
            System.exit(1);
        }

        //Assigns arguments passed in
        String infilename = args[0];
        String outfilename = args[1];

        //Opens, Reads, and Closes Serialized File
        StudentDatabaseSerializableFileReader.openFile(infilename);
        StudentDatabase db = StudentDatabaseSerializableFileReader.readObjects();
        StudentDatabaseSerializableFileReader.closeFile();

        //Sorts Database by GPA
        db.sortByGPA();

        //Opens, Writes, and Closes CSV File
        StudentDatabaseCSVFileWriter.openFile(outfilename);
        StudentDatabaseCSVFileWriter.writeData(db);
        StudentDatabaseCSVFileWriter.closeFile();
    }//End Main Method

}//End ReadSortAndSerializeStudentDatabase
