import java.util.ArrayList;
/**
* A simple program that takes in a
* regular txt file that contains a student database
* and converts it to a Serialized file
*
* @author Silvino Pina
*
*/
public class BuildSortAndSerializeStudentDatabase {

    //Main method that converts a CSV file into a Serialized file
    public static void main(String[] args) {
        //Checks if correct amount of arguments are passed in
        if (args.length != 2) {
            System.err.println("Usage: BuildSortAndSerializeStudentDatabase inputfilename outputfilename");
            System.exit(1);
        }
        //Assigns arguments passed in
        String infilename = args[0];
        String outfilename = args[1];

        //Opens, Reads, and Closes CSV File
        StudentDatabaseCSVFileReader.openFile(infilename);
        StudentDatabase db = StudentDatabaseCSVFileReader.readData();
        StudentDatabaseCSVFileReader.closeFile();

        //Sorts Database by GPA
        db.sortByGPA();

        //Opens, Writes, and Closes Serialized File
        StudentDatabaseSerializableFileWriter.openFile(outfilename);
        StudentDatabaseSerializableFileWriter.writeData(db);
        StudentDatabaseSerializableFileWriter.closeFile();
    }//end main method
}//end BuildSortAndSerializeStudentDatabase
