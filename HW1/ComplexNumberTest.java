import org.junit.*;
import static org.junit.Assert.*;
/**
 * Test class for class <code>ComplexNumber</code>. 
 *
 *  
 *
 * @author	Silvino Pina
 * @version	6 February 2018
 */ 


public class ComplexNumberTest{
	
	
	private ComplexNumber complex1;
	private ComplexNumber complex2;
	private ComplexNumber complex3;
	private ComplexNumber complex4;
	private ComplexNumber complex5;

	/**
	 * Creates test numbers before each test is run. 
	 */ 
	@Before
	public void start(){
		complex1 = new ComplexNumber(0,-5);
		complex2 = new ComplexNumber(-3, 8);
		complex3 = new ComplexNumber(5,-8);
		complex4 = new ComplexNumber(7,0);
		complex5 = new ComplexNumber(0,0);
	}//End Start Method.

	/**
	 * Tests the <code>getA</code> method of <code>ComplexNumber</code>. 
	 */ 
	@Test
	public void testGetA(){
		assertEquals(0, complex1.getA(), 0.0f);
		assertEquals(-3, complex2.getA(), 0.0f);
		assertEquals(5, complex3.getA(), 0.0f);
		assertEquals(7, complex4.getA(), 0.0f);
	}//End Test Get A Method.

	/**
	 * Tests the <code>getB</code> method of <code>ComplexNumber</code>. 
	 */ 
	@Test
	public void testGetB(){
		assertEquals(-5, complex1.getB(), 0.0f);
		assertEquals(8, complex2.getB(), 0.0f);
		assertEquals(-8, complex3.getB(), 0.0f);
		assertEquals(0, complex4.getB(), 0.0f);
	}//End Test Get B Method.

	/**
	 * Tests the <code>equals</code> method of <code>ComplexNumber</code>. 
	 * The tests should ensure that all aspects of that contract are fulfilled. 
	 */ 
	@Test
	public void testEquals(){
		ComplexNumber nullNum = null;
		assertFalse(complex4.equals(null));
		assertTrue(complex1.equals(complex1));
		assertFalse(complex2.equals(complex4));
		assertFalse(complex3.equals("Imaginary"));
		assertEquals(new ComplexNumber(0,-5), complex1);
		assertEquals(new ComplexNumber(-3, 8), complex2);
		assertEquals(new ComplexNumber(5, -8), complex3);
		assertEquals(new ComplexNumber(7, 0), complex4);
		assertEquals(complex4, new ComplexNumber(7, 0));
		assertEquals(complex3, new ComplexNumber(5, -8));
		assertEquals(complex2, new ComplexNumber(-3, 8));
		assertEquals(complex1, new ComplexNumber(0,-5));
	}//End Test Equals Method.


	/**
	 * Tests the <code>Add</code> method of <code>ComplexNumber</code>. 
	 * This test uses different methods for comparing values,
	 * including using the other methods of 
	 * <code>ComplexNumber</code>. 
	 */ 

	@Test
	public void testAdd(){
		assertEquals("-3.0 + 3.0i", complex1.add(complex2).toString());
		assertEquals(new ComplexNumber(12, -8), complex3.add(complex4));
		assertEquals(2, complex2.add(complex3).getA(), 0.0f);
		assertEquals(0, complex2.add(complex3).getB(), 0.0f);
		assertEquals("4.0 + 8.0i", complex2.add(complex4).toString());
		assertEquals(new ComplexNumber(7, -5),complex1.add(complex4));
	}//End Test Addition Method.

	/**
	 * Tests the <code>Subtract</code> method of <code>ComplexNumber</code>. 
	 * This test uses different methods for comparing values,
	 * including using the other methods of 
	 * <code>ComplexNumber</code>. 
	 */ 
	@Test
	public void testSub(){
		assertEquals(3, complex1.subtract(complex2).getA(), 0.0f);
		assertEquals("-2.0 - 8.0i", complex3.subtract(complex4).toString());
		assertEquals(new ComplexNumber(-5, 3), complex1.subtract(complex3));
		assertEquals(8 , complex4.subtract(complex3).getB(), 0.0f);
		assertEquals("8.0 - 16.0i", complex3.subtract(complex2).toString());
	}//End Test Subtraction Method.
	
	/**
	 * Tests the <code>Subtract</code> method of <code>ComplexNumber</code>. 
	 * This test uses different methods for comparing values,
	 * including using the other methods of 
	 * <code>ComplexNumber</code>. 
	 */ 
	@Test
	public void testMultipy(){
		assertEquals(40, complex1.multiply(complex2).getA(), 0.0f);
		assertEquals("35.0 - 56.0i", complex3.multiply(complex4).toString());
		assertEquals(new ComplexNumber(-40, -25), complex1.multiply(complex3));
		assertEquals(-56 , complex4.multiply(complex3).getB(), 0.0f);
		assertEquals("49.0 + 64.0i", complex3.multiply(complex2).toString());
	}//End Test Multiply Method

	/**
	 * Tests the <code>Divide</code> method of <code>ComplexNumber</code>. 
	 * and ensures an exception is thrown when dividing by 0 
	 */ 
	@Test
	public void testDivide(){
		assertEquals(-0.547, complex1.divide(complex2).getA(), 0.1f);
		assertEquals("0.71428573 - 1.1428572i" ,complex3.divide(complex4).toString());
		assertEquals(new ComplexNumber(0.4494382f, -0.28089887f ), complex1.divide(complex3));
		assertEquals(0.6292135, complex4.divide(complex3).getB(), 0.1f);
		assertEquals("-1.0821918 - 0.21917808i", complex3.divide(complex2).toString());
		try{
			complex1.divide(complex5); 
		} catch (ArithmeticException e){
			assertEquals("Division by zero is not tolerated !", e.getMessage());
		}
	}//End Test Divide Method.

	/**
	 * Tests the <code>toString</code> method of <code>ComplexNumber</code>.
	 */ 
	@Test
	public void testToString(){
		assertEquals("0.0 - 5.0i",complex1.toString());
		assertEquals("-3.0 + 8.0i",complex2.toString());
		assertEquals("5.0 - 8.0i",complex3.toString());
		assertEquals("7.0 + 0.0i",complex4.toString());
	}//End Test ToString Method.
}//End Complex Number Test Class
