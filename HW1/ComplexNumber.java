import java.lang.ArithmeticException;
/**
* A class to model a Complex Number. Methods include add, 
* subtract, multiply, divide, toString, and the 
* appropriate getters.
*
*
*
*@author Silvino Pina
*@version February 1 2017
*
*
*/


public class ComplexNumber{
	private float a;
	private float b;

	/**
	 * Initializes <code>a</code> and 
	 * <code>b</code> to values passed 
	 * as arguments. 
	 * 
	 * @param	a	the new Complex Number's Real Number
	 * @param	b 	the new Complex Number's Imaginary Value
	 */ 
	public ComplexNumber(float a, float b){
			this.a = a;
			this.b = b;

	}//end ComplexNumber Constructor.

	/** 
	 * Returns the <code>Real Number</code> of 
	 * the <code>ComplexNumber</code>. 
	 *
	 * @return	this.a
	 */ 
	public float getA(){
		return this.a;
	}//end getA method

	/** 
	 * Returns the <code>Imaginary Number</code> of 
	 * the <code>ComplexNumber</code>. 
	 *
	 * @return	this.b
	 */ 
	public float getB(){
		return this.b;
	}//end getB method

	/**
	 * Adds the <code>ComplexNumber</code> to another 
	 * provided as an argument. Returns the sum as 
	 * a new <code>ComplexNumber</code> instance. 
	 * 
	 * @param	otherComplexNumber the other addend
	 * @return	a new ComplexNumber that is the sum
	 */ 
	public ComplexNumber add(ComplexNumber otherComplexNumber){
		float realSum = this.a + otherComplexNumber.a;
		float complexSum = this.b + otherComplexNumber.b;
		return new ComplexNumber(realSum, complexSum);
	}//end complexnumber add method

	/**
	 * The <code>ComplexNumber</code> is subtracted by another 
	 * provided as an argument. Returns the difference as 
	 * a new <code>ComplexNumber</code> instance. 
	 * 
	 * @param	otherComplexNumber the other subtrahend
	 * @return	a new ComplexNumber that is the difference
	 */ 
	public ComplexNumber subtract(ComplexNumber otherComplexNumber){
		float realDiff = this.a - otherComplexNumber.a;
		float complexDiff = this.b - otherComplexNumber.b;
		return new ComplexNumber (realDiff, complexDiff);
	}//end complexnumber subtract method

	/**
	 * Multiplies the <code>ComplexNumber</code> by multiple 
	 * provided as an argument. Returns the quotient 
	 * as a new <code>ComplexNumber</code> instance. 
	 * 
	 * @param	otherComplexNumber	the multiple ComplexNumber
	 * @return	a new ComplexNumber that is the Product
	 */ 
	public ComplexNumber multiply(ComplexNumber otherComplexNumber){
		float realProduct = this.a * otherComplexNumber.a - this.b * otherComplexNumber.b;
		float complexProduct = this.a * otherComplexNumber.b + this.b *otherComplexNumber.a;
		return new ComplexNumber (realProduct, complexProduct);
	}//end complexnumber multiply method

	/**
	 * Divides the <code>ComplexNumber</code> by divisor 
	 * provided as an argument. Returns the quotient 
	 * as a new <code>ComplexNumber</code> instance. 
	 * 
	 * @param	otherComplexNumber	the divisor ComplexNumber
	 * @throws	ArithmeticException
	 * @return	a new ComplexNumber that is the quotient
	 */ 
	public ComplexNumber divide(ComplexNumber otherComplexNumber){
		float squared = (otherComplexNumber.a * otherComplexNumber.a + otherComplexNumber.b * otherComplexNumber.b);
		//Attempt Division by 0
		if (squared == 0){
			throw new ArithmeticException("Division by zero is not tolerated !");
		} 
		
		else{
			float realQoutient = ((this.a * otherComplexNumber.a) + (this.b * otherComplexNumber.b ))/squared;
			float complexQoutient = ((this.b * otherComplexNumber.a) - (this.a * otherComplexNumber.b ))/squared;
			return new ComplexNumber (realQoutient , complexQoutient);
		}
	}//end complexnumber divide method

	/**
	 * Returns a String representation of the 
	 * <code>ComplexNumber</code>. 
	 * 
	 * @return	a String representation of this
	 */ 
	@Override
	public String toString(){
		if(this.b < 0 ){
			return this.a + " - " + Math.abs(this.b) + "i";
		}
		return this.a + " + " + this.b + "i";
	}

	/**
	*indicates wether <code>ComplexNumber</code> is
	* "equal to" another <code>ComplexNumber</code>
	*
	* @return true if values are equal, false otherwise
	*/
	@Override
	public boolean equals(Object otherComplexNumber){
		//Check for null refrence
		if ((otherComplexNumber != null) && 
			(this.getClass().equals(otherComplexNumber.getClass()))) {
			 
			
			// Compare the two complex numbers value. 
			if ((this.a == ((ComplexNumber)otherComplexNumber).getA()) && 
				(this.b == ((ComplexNumber)otherComplexNumber).getB())) {
				return true; 
			} 
				
			else {
				return false; 
			}
		} 
		
		else {
			return false; 
		} 
	} // end method equals
}//end class 