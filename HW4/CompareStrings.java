import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;
/**
*A class that compares to string and finds the minimum
*in an array of strings and includes junit test.
*
*
*
*@author silvino pina
*@version 04/03/18
*
*/

public class CompareStrings{
  /**
  * Instance variables for test procedure
  *
  *
  */
  private String string1;
  private String string2;
  private String string3;
  private String string4;
  private String string5;
  private String string6;

  private ArrayList<String> stringTest1;
  private ArrayList<String> stringTest2;

  /**
	 * Creates test string and arrays before each test is run.
	 */
  @Before
  public void setUp(){
    string1 = "One";
    string2 = "Two";
    string3 = "Three";
    string4 = "Four";
    string5 = "Five";
    string6 = "Six";

    stringTest1 = new ArrayList<String>();
    stringTest1.add("Zach");
    stringTest1.add("Fifi");
    stringTest1.add("Alan");
    stringTest1.add("Bob");
    stringTest1.add("Blake");

    stringTest2 = new ArrayList<String>();
    stringTest2.add("orange");
    stringTest2.add("apple");
    stringTest2.add("apricot");
    stringTest2.add("tomato");
    stringTest2.add("avocado");
    stringTest2.add("peach");

  }//end setUp
  /**
   * Tests the <code>compareTo</code> method of <code>CompareStrings</code>.
   */
  @Test
  public void testCompareTo(){
    assertEquals(-1,compareTo(string5,string3));
    assertEquals(1,compareTo(string6,string1));
    assertEquals(0,compareTo(string3,string3));
    assertEquals(-1,compareTo(string1,string6));
    assertEquals(1,compareTo(string2,string3));
  }

  /**
	 * Tests the <code>findMinimum</code> method of <code>CompareStrings</code>.
	 */
  @Test
  public void testFindMinimum(){
    assertEquals("Alan",findMinimum(stringTest1));
    assertEquals("apple",findMinimum(stringTest2));
  }//end testFindMinimum

  /**
  * method that will compare to string and return
  * and integer to idicate its alphabetical order
  *
  * @param s1 one of the strings being compared
  * @param s2 one of the strings being compared
  *
  * @return -1 if s1 comes before
  * @return 0 if s1 is equal to s2
  * @return 1 if s1 comes after
  */
  public static int compareTo(String s1, String s2){
      return compareTo(s1,s2,0);

    }//end compareTo

    /**
    * method that will compare to string and return
    * and integer to idicate its alphabetical order
    *
    * @param s1 one of the strings being compared
    * @param s2 one of the strings being compared
    * @param index starting vaule used for Char comparison
    *
    * @return -1 if s1 comes before
    * @return 0 if s1 is equal to s2
    * @return 1 if s1 comes after
    */
    private static int compareTo(String s1, String s2, int index) {

      if(s1.length() == index && s2.length() == index) {
           return 0;
       }

       if(s1.length() == index && s2.length() > index) {
           return -1;
       }

       if(s1.length() > index && s2.length() == index) {
           return 1;
       }

       if(Character.toLowerCase(s1.charAt(index))< Character.toLowerCase(s2.charAt(index))) {
           return -1;
       }

       else if(Character.toLowerCase(s1.charAt(index))> Character.toLowerCase(s2.charAt(index))) {
           return 1;
      }
       return compareTo(s1,s2,index+1);

   } //end compareTo

   /**
   * Method that will locate the minimum value in
   * the ArrayList
   * @param stringArray an array to look for minimum
   * @return first alphabetical ordered string
   */
   public static String findMinimum(ArrayList<String> stringArray){
     return findMinimum(stringArray,1,0);
   }

   /**
   * Method that will locate the minimum value in
   * the ArrayList
   * @param stringArray an array to look for minimum
   * @param numStrings number of strings
   * @param start starting point
   * @return first alphabetical ordered string
   */
   private static String findMinimum(ArrayList<String> stringArray, int numStrings,int start){
     String min = stringArray.get(start);
     while(numStrings<stringArray.size()){
       int order = CompareStrings.compareTo(min,stringArray.get(numStrings));
       if(order == 1){
         min = stringArray.get(numStrings);
         start = numStrings;
       }
       return findMinimum(stringArray,numStrings+1,start);
     }
     return min;
  }//end findMinimum
}//end class
