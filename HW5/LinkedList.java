public class LinkedList<T>  {


    Node<T> itsFirstNode;
    Node<T> itsLastNode;
    private int size;


    public LinkedList() {
        itsFirstNode = null;
        itsLastNode = null;
        size = 0;
    }

    public int size() {
        return this.size;
    }

    public Iterator<T> getIterator() {
        return new Iterator<T>(this);
    }

    /**
    * adds a new element to the linked list
    *
    * @param element the element added
    */
    public void add(T element) {

        Node<T> node = new Node<T>(element);

        if (itsFirstNode == null) {
            itsFirstNode = node;
            itsLastNode = node;
        }
        else {
            itsLastNode.setNextNode(node);
            node.setPriorNode(itsLastNode);
            //node.setNextNode(null);
            itsLastNode = node;

        }
        size++;
    }

    /**
    * adds a new element to the linked list in
    * specific spot of the linked list.
    *
    * @param element the element added
    * @param index position where the element will be added
    */
    public void add(T element, int index) {
        int counter = 0;
        Node<T> newNode = new Node<T>(element);
        Node<T> current = itsFirstNode;
        Node<T> next = null;
        while (current != null ) {
            if (counter == index - 1 )
                break;
            current = current.getNextNode();
            counter++;
        }
        next = current.getNextNode();
        next.setPriorNode(newNode);
        newNode.setNextNode(current.getNextNode());
        current.setNextNode(newNode);
        newNode.setPriorNode(current);
        size++;
    }

    public T get(int index) {
        int counter = 0;
        Node<T> current = itsFirstNode;
        while (current != null ) {
            if (counter == index)
                break;
            current = current.getNextNode();
            counter++;
        }
        return current.getData();
    }

    /**
    * checks to see if a certain element
    * is within the linkedlist
    *
    * @param element the element searching for
    * @return boolean true if element is within List
    * @return boolean false if element is not whitin the list
    */
    public boolean contains(T element) {
        Node<T> pointer = itsFirstNode;

        while(pointer != itsLastNode.getNextNode()){
            if(pointer.getData().equals(element)){

              return true;

            }
              pointer = pointer.getNextNode();

        }

        return false;
    }

    /**
    * checks to see if a certain element
    * is within the linkedlist and returns the
    * index
    *
    * @param element the element searching for
    * @return index where the element is located
    * @return -1 if element is not found
    */
    public int indexOf(T element) {
      int index = 0;
      Node<T> pointer = itsFirstNode;

      while(pointer != itsLastNode.getNextNode()){
          if(pointer.getData().equals(element)){
            return index;

          }
            index ++;
            pointer = pointer.getNextNode();
      }

      return -1;
    }

    /**
    * checks to see if a certain element
    * is within the linkedlist and returns an
    * iterator to that index
    *
    * @param element the element searching for
    * @return Iterator to location of the element
    * @return null if element is not present
    */
    public Iterator<T> iteratorAt(T element) {
      Iterator<T> iter = new Iterator<T>(this);
      int counter = 0;
      int index = indexOf(element);
      Node<T> current = itsFirstNode;
      if (index != -1){
        while (current != null ) {
            if (counter == index)
                break;
            current = current.getNextNode();
            counter++;
        }
          iter.setMyCurrentNode(current.getNextNode());
          return iter;
      }
        return null;
    }


    public String toString() {
        String returnVal = "";
        Node<T> current = itsFirstNode;
        if (size != 0 ) {
            while (current != null ) {
                returnVal += current.toString();
                returnVal += "\n";
                current = current.getNextNode();
            }
        }
        return returnVal;
    }  // end toString

    class Node<T> {

        private T data;
        private Node<T> itsNext;
        private Node<T> itsPrior;

        public Node(T data) {
            itsNext = null;
            itsPrior = null;
            this.data = data;
        }


        public T getData() {
            return this.data;
        }

        public Node<T> getNextNode() {
            return itsNext;
        }

        /**
        * returns prior node
        * @return Node
        */
        public Node<T> getPriorNode() {
            return itsPrior;
        }


        public void setNextNode(Node<T> next) {
            itsNext = next;
        }

        /**
        * sets prior node
        * @param prior node to be set as prior
        */
        public void setPriorNode(Node<T> prior) {
            itsPrior = prior;
        }


        public String toString() {
            return data.toString();
        }

    }  // end of Node<T>
}
