import static org.junit.Assert.*;
import org.junit.*;
/*
*
* A test for the <code>LinkedList<code>
* and the <code>Iterator<code>
*
* @author Silvino Pina
*/
public class LinkedListTester{

      private static Dog fifi;
      private static Dog butch;
      private static Dog leonard;
      private static Dog spot;
      private static Dog jack;
      private static Dog spike;
      private static LinkedList<Dog> myDogList;
      private static Iterator<Dog> iter1;
      private static Iterator<Dog> iter2;
 /*
  * Creates Test objects before each test is run
  *
  *
  */
 @BeforeClass
 public static void setup() {
       fifi  = new Dog("Fifi", 12, 8);
       butch = new Dog("Butch", 10, 10);
       leonard = new Dog("Leonard", 22, 13);
       spot = new Dog("Spot", 17, 9);

       myDogList = new LinkedList<Dog>();
       myDogList.add(fifi);
       myDogList.add(butch);
       myDogList.add(leonard);
       myDogList.add(spot);

       iter1 = myDogList.getIterator();

 } // end setup method

 /**
 * Test the <code>hasNext<code> method of
 * <code>Iterator<code>
 *
 */

 @Test
 public void testHasNext(){
   while (iter1.hasNext() == true){
     assertTrue(iter1.hasNext() == true);
   }
   assertTrue(iter1.hasNext() == false);
 }

 /**
 * Test the <code>hasPrior<code> method of
 * <code>Iterator<code>
 *
 */
 @Test
 public void testHasPrior(){
   while(iter1.hasPrior() == true){
     assertTrue(iter1.hasPrior() == true);
   }
   assertTrue(iter1.hasPrior() == false);
 }

 /**
 * Test the <code>get<code> method of
 * <code>LinkedList<code>
 *
 */
 @Test
 public void testGet(){
   assertEquals(fifi , myDogList.get(0));
   assertEquals(jack , myDogList.get(1));
   assertEquals(butch , myDogList.get(2));
   assertEquals(leonard , myDogList.get(3));
   assertEquals(spot , myDogList.get(4));
   assertEquals(spike , myDogList.get(5));
 }
 /**
 * Test the <code>add<code> method of
 * <code>LinkedList<code>
 *
 */
 @Test
 public void testAdd(){
   jack = new Dog("Jack", 18, 18);
   assertTrue(myDogList.contains(jack) == false);
   myDogList.add(jack,1);
   assertTrue(myDogList.contains(jack) == true);
   spike = new Dog("Spike", 20, 12);
   assertTrue(myDogList.contains(spike) == false);
   myDogList.add(spike);
   assertTrue(myDogList.contains(spike) == true);

 }

 /**
 * Test the <code>contains<code> method of
 * <code>LinkedList<code>
 *
 */
 @Test
 public void testContains(){
   assertTrue(myDogList.contains(fifi));
   assertTrue(myDogList.contains(butch));
   assertTrue(myDogList.contains(leonard));
   assertTrue(myDogList.contains(jack));
   assertTrue(myDogList.contains(spot));
   assertTrue(myDogList.contains(spike));
 }

 /**
 * Test the <code>indexOf<code> method of
 * <code>LinkedList<code>
 *
 */
 @Test
 public void testIndexOf(){
   assertEquals(0, myDogList.indexOf(fifi));
   assertEquals(1, myDogList.indexOf(jack));
   assertEquals(2, myDogList.indexOf(butch));
   assertEquals(3, myDogList.indexOf(leonard));
   assertEquals(4, myDogList.indexOf(spot));
   assertEquals(5, myDogList.indexOf(spike));

 }

 /**
 * Test the <code>next<code> method of
 * <code>Iterator<code>
 *
 */
 @Test
 public void testPrior(){
    assertEquals(spot , iter1.prior());
    assertEquals(leonard, iter1.prior());
    assertEquals(butch, iter1.prior());
    assertEquals(jack , iter1.prior());
    assertEquals(fifi , iter1.prior());

 }

 /**
 * Test the <code>next<code> method of
 * <code>Iterator<code>
 *
 */
 @Test
 public void testNext(){
   //starts the iterator at begining
    assertEquals(fifi , iter1.next());
    assertEquals(jack , iter1.next());
    assertEquals(butch , iter1.next());
    assertEquals(leonard , iter1.next());
    assertEquals(spot , iter1.next());
    assertEquals(spike , iter1.next());


 }
 /**
 * Test the <code>iteratorAt<code> method of
 * <code>LinkedList<code>
 *
 */
 @Test
 public void testIteratorAt(){
   assertTrue(myDogList.iteratorAt(fifi).next().equals(jack));
   assertTrue(myDogList.iteratorAt(jack).next().equals(butch));
   assertTrue(myDogList.iteratorAt(butch).next().equals(leonard));
   assertTrue(myDogList.iteratorAt(leonard).next().equals(spot));
   assertTrue(myDogList.iteratorAt(spot).next().equals(spike));

 }
}
