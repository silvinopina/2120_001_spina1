import java.util.*;
/**
* This class represents a object that can have one or more
* observers. Observers is an interface impelmented by the class dog 
* walker.
*
*@author Silvino Pina
*
*/

public class MyObservable{

    private boolean changed = false;
    private MyObserver[] observers = new MyObserver[20];
    
    private int tracker = 0; 


    //*Constructs a MyObservable array.
    public MyObservable() {
    }
   
   /**
   * Adds a MyObserver to the array of observers
   * that is different from the observers
   * in the set 
   *
   *@param o the MyObserver to be added
   *@throws NullPointerException if param is null
   */
    public void addMyObserver(MyObserver o) {
        
        if (o == null)
            throw new NullPointerException();

            observers[tracker] = o;
            tracker ++;
    }

    /**
     * If this object has changed, as designated by the
     * <code>hasChanged</code> method, notify all of its observers
     * and call the <code>clearChanged</code> method to indicate
     * that this object has no longer changed.
     */
    public void notifyMyObservers() {
        notifyMyObservers(null);
    }

    /**
     * If this object has changed, as indicated by the
     * <code>hasChanged</code> method, then notify all of its observers
     * and then call the <code>clearChanged</code> method to indicate
     * that this object has no longer changed.
     * @param   arg   any object.
     */
    public void notifyMyObservers(Object arg) { 
            if (!changed){
                return;
            }
            
       
    
        for(int i = 0; i < tracker; i++){
            observers[i].update(this, arg);

            //clearChanged();
        }
        

    }

    
    /**
    * Indicates that the object has been changed.
    */
    protected void setChanged() {
        changed = true;
    }

    /**
    * Indicates that the object has no longer been changed.
    */
    protected  void clearChanged() {
        changed = false;
    }

    /**
    * Test wether the object has changed 
    *
    *@return true if <code>setChanged<code> has been called
    * false if <code>clearChanged<code> has been called
    *
    *
    */
    public boolean hasChanged() {
        return changed;
    }

    /** 
    * Returns the number of observes in the list.
    *
    *@return number of observers
    */
    public int countObservers() {
        int count = 0;
        for(int i = 0; i < observers.length; i++)
        if (observers[i] != null)
            ++count;
    return count;
    }
    
    /**
    * Deletes all observers from list
    */
    public void deleteObservers (){
    	observers = null;
    }

}//end class