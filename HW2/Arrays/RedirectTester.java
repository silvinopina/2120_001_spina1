
import java.io.ByteArrayOutputStream; 
import java.io.PrintStream;
import static org.junit.Assert.*; 
import org.junit.*;
/**
* A test for the <code>MyObservable<code> and <code>Myobserver<code>
*
*@author Silvino Pina
*/

public class RedirectTester{

	private ByteArrayOutputStream output = new ByteArrayOutputStream(); 
        
        private DogWalker bob;
        private DogWalker rick; 
        private DogWalker morty;

        private Dog butch;
        private Dog fifi;
        private Dog spike;
     
        /*
        * Creates Test objects before each test is run
        *
        *
        */
	@Before
	public void setup() {
        System.setOut(new PrintStream(output));
        bob = new DogWalker("Bob");
        rick = new DogWalker("Rick");
        morty = new DogWalker("Morty");

        butch = new Dog("Butch");
        fifi  = new Dog("Fifi");
        spike = new Dog("Spike");

        butch.addMyObserver(bob);
        fifi.addMyObserver(bob);
        fifi.addMyObserver(rick);
        spike.addMyObserver(rick);
        spike.addMyObserver(morty);

	} // end setup method

        /**
        * Test the <code>notifyMyobserves<code> method of
        * <code>MyObservable<code>
        *
        *
        */
	@Test
	public void printOut() {
		butch.poop();
		assertEquals("I'm pooping!!" + "\n" + "Hey, Bob has been notified that Butch has pooped!" + "\n", output.toString());
		output.reset();

                fifi.poop();
                assertEquals("I'm pooping!!" + "\n" + "Hey, Bob has been notified that Fifi has pooped!" + "\n"+
                "Hey, Rick has been notified that Fifi has pooped!" +"\n" , output.toString());
                output.reset();

                spike.poop();
                assertEquals("I'm pooping!!" + "\n" + "Hey, Rick has been notified that Spike has pooped!" + "\n"+
                "Hey, Morty has been notified that Spike has pooped!" +"\n" , output.toString());
                output.reset();
	}//end print out method


           /**
        * Test the <code>countObservers<code> method of
        * <code>MyObservable<code>
        *
        *
        */
        @Test
        public void testCount(){
                assertEquals(1 , butch.countObservers());
                assertEquals(2 , fifi.countObservers());
                assertEquals(2 , spike.countObservers());
        }//end testCount

} // end class RedirectTester