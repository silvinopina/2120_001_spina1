public class LafDogPark {

    public static void main(String[] args) {

        DogWalker bob = new DogWalker("Bob");
        DogWalker rick = new DogWalker("Rick");
        DogWalker morty = new DogWalker("Morty");

        Dog butch = new Dog("Butch");
        Dog fifi  = new Dog("Fifi");
        Dog spike = new Dog("Spike");

        butch.addMyObserver(bob);
        fifi.addMyObserver(bob);
        fifi.addMyObserver(rick);
        spike.addMyObserver(rick);
        spike.addMyObserver(morty);

        System.out.println("Butch poops");
        butch.poop();
        System.out.println("Fifi poops");
        fifi.poop();
        System.out.println("Spike poops");
        spike.poop();

    }


} // end class
