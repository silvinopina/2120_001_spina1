import java.util.*;
/**
*
*
*@author Silvino Pina
*
*/
public interface MyObserver{
	/**
	*
	*
	*
	*@param o the observable object
	*@param arg argument passed to <code>notifyObservers<code>
	*/
	public void update (MyObservable o, Object arg);


}//end class