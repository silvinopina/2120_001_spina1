import java.util.*;

/**
* A class that creates a dog object to extend
* the class <code>MyObservable<code>
*
*
*@Author Silvino Pina
*
*
*/


public class Dog extends MyObservable {

    private String name;
    private boolean hasPooped;

    /**
    *A constructor for the object dog
    *
    *
    *@param name A string that is taken in
    *
    */
    public Dog(String name) {

        this.name = name;

    }// end constructor

    /**
    * A method thst returns the name of the dog
    *
    *@return name 
    *
    */
    public String getName() {
        return this.name;
    }//end getname

    /**
    * A method that simulates a dog pooping
    * and notfiies the dogwalker
    *
    */
    public void poop() {
        System.out.println("I'm pooping!!");
        hasPooped = true;
        setChanged();
        notifyMyObservers(name + " has pooped!");
    }// end poop method

}//end class
