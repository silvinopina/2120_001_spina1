import java.io.ByteArrayOutputStream; 
import java.io.PrintStream;
import static org.junit.Assert.*; 
import org.junit.*;
/**
* A test for the <code>MyObservable<code> and <code>Myobserver<code>
*
*@author Silvino Pina
*/

public class RedirectTester{

        private ByteArrayOutputStream output = new ByteArrayOutputStream(); 
        
        private DogWalker bob;
        private DogWalker rick; 
        private DogWalker morty;

        private Dog butch;
        private Dog fifi;
        private Dog spike;
     
        /*
        * Creates Test objects before each test is run
        *
        *
        */
        @Before
        public void setup() {
        System.setOut(new PrintStream(output));
        bob = new DogWalker("Bob");
        rick = new DogWalker("Rick");
        morty = new DogWalker("Morty");

        butch = new Dog("Butch");
        fifi  = new Dog("Fifi");
        spike = new Dog("Spike");

        butch.addMyObserver(bob);
        fifi.addMyObserver(bob);
        fifi.addMyObserver(rick);
        spike.addMyObserver(rick);
        spike.addMyObserver(morty);

        } // end setup method

        /**
        * Test the <code>notifyMyobserves<code> method of
        * <code>MyObservable<code>
        *
        *
        */
        @Test
        public void printOut() {
                butch.poop();
                assertEquals("I'm pooping!!" + "\n" + "Hey, Bob has been notified that Butch has pooped!" + "\n", output.toString());
                output.reset();

                fifi.poop();
                assertEquals("I'm pooping!!" + "\n" + "Hey, Rick has been notified that Fifi has pooped!" + "\n"+
                "Hey, Bob has been notified that Fifi has pooped!" +"\n" , output.toString());
                output.reset();

                spike.poop();
                assertEquals("I'm pooping!!" + "\n" + "Hey, Morty has been notified that Spike has pooped!" + "\n"+
                "Hey, Rick has been notified that Spike has pooped!" +"\n" , output.toString());
                output.reset();
        }//end print out method


        /**
        * Test the <code>countObservers<code> method of
        * <code>MyObservable<code>
        *
        *
        */
        @Test
        public void testCount(){
                assertEquals(1 , butch.countObservers());
                assertEquals(2 , fifi.countObservers());
                assertEquals(2 , spike.countObservers());
        }//end testCount

        /**
        * Test the <code>deleteObservers<code>
        * method of <code>MyObservable<code>
        *
        */
        @Test
        public void testClear(){
                butch.deleteObservers();
                assertEquals(0 , butch.countObservers());

                fifi.deleteObservers();
                assertEquals(0 , fifi.countObservers());

                spike.deleteObservers();
                assertEquals(0 , spike.countObservers());
        }//end testClear

        /**
        * Test the <code>deleteObserver<code>
        * method of <code>MyObservable<code>
        *
        */
        @Test
        public void testDelete(){
                butch.deleteObserver(bob);
                assertEquals(0 , butch.countObservers());

                fifi.deleteObserver(rick);
                assertEquals(1 , fifi.countObservers());

                spike.deleteObserver(morty);
                assertEquals(1 , spike.countObservers());
        }//end testDelete



} // end class RedirectTester