import java.util.*;
/**
* A class that creates a dog walker
* that implements the MyObserver interface
*
*@author Silvino Pina
*
*/
public class DogWalker implements MyObserver {

    private String name;

     /**
    *A constructor for the object dogwalker
    *
    *
    *@param name A string that is taken in
    *
    */
    public DogWalker(String name) {
        this.name = name;
    }//end constructor

    /**
    * A class that updates the dogwalker
    * whenever it is called from the <code>Dog<code>
    * class
    *
	*@param o an observable that will be notified
	*@param arg an argument that will be to notify observer
	*/
    public void update(MyObservable o, Object arg) {

        System.out.println("Hey, " + name + " has been notified that " + arg);

    }//end update method

}//end class
